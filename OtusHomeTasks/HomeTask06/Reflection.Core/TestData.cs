﻿namespace Reflection.Core
{
    public class TestData
    {
        public string PublicProperty1 { get; set; } = string.Empty;

        public int PublicProperty2 { get; set; } = 10;

        public bool PublicProperty3 { get; set; } = true;
    }
}
