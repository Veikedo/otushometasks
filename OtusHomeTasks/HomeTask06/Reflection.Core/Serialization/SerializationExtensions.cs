﻿using System;
using System.Linq;

namespace Reflection.Core.Serialization
{
    public static class SerializationExtensions
    {
        public static string SerializeToCsv(this object sender, string separator = ",")
        {
            if (sender == null)
            {
                throw new ArgumentNullException(nameof(sender));
            }

            Type t = sender.GetType();
            var props = t.GetProperties();
            if (props?.Length == 0)
            {
                return string.Empty;
            }

            string header = string.Join(separator, props.Select(f => f.Name).ToArray());
            string values = string.Join(separator, props.Select(p => p.GetValue(sender)).ToArray());
            return header + Environment.NewLine + values;
        }

        public static T DeserializeFromCsv<T>(this string csv, string separator = ",") where T : new()
        {
            if (string.IsNullOrWhiteSpace(csv))
            {
                throw new ArgumentException("CSV-string cannot be null or whitespace.");
            }

            var csvData = csv.Split(Environment.NewLine);
            if (csvData?.Length != 2)
            {
                throw new ArgumentException("CSV-string must contain two lines - properties names and values.");
            }

            var csvProps = csvData[0].Split(separator, StringSplitOptions.RemoveEmptyEntries);
            var csvValues = csvData[1].Split(separator, StringSplitOptions.None);
            T result = new T();
            if (csvProps.Length != csvValues.Length)
            {
                throw new ArgumentException("Number of properties is not equal to the number of values in CSV-string.");
            }

            var type = result.GetType();
            for (int i = 0; i < csvProps.Length; i++)
            {
                var propertyInfo = type.GetProperty(csvProps[i]);
                var value = Convert.ChangeType(csvValues[i], propertyInfo.PropertyType);
                propertyInfo.SetValue(result, value);
            }

            return result;
        }
    }
}
