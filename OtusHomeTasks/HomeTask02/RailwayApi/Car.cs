﻿using System;

namespace RailwayApi
{
    /// <summary>
    /// Train car model.
    /// </summary>
    public class Car
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Car() : this(0) { }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="capacity">Car capacity.</param>
        public Car(uint capacity)
        {
            Capacity = capacity;
        }

        /// <summary>
        /// Car passenger capacity.
        /// </summary>
        public uint Capacity { get; }

        /// <summary>
        /// Is car for passengers.
        /// </summary>
        public bool IsPassenger => Capacity > 0;

        public override bool Equals(object obj)
        {
            Car car = obj as Car;
            return !ReferenceEquals(null, car) && Capacity == car.Capacity;
        }

        /// <summary>
        /// GetHashCode implementation by Jon Skeet.
        /// </summary>
        /// <returns>HashCode.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions.
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Capacity) ? Capacity.GetHashCode() : 0);
                return hash;
            }
        }

        public override string ToString()
        {
            return $"Passenger capaity = {Capacity}";
        }
    }
}
