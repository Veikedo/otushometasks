﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RailwayApi.Extensions
{
    /// <summary>
    /// Extensions for <see cref="Train"/>.
    /// </summary>
    public static class TrainExtensions
    {
        /// <summary>
        /// Get only non-passenger cars (cars with zero capacity).
        /// </summary>
        /// <param name="source">Train.</param>
        /// <returns>Non-passenger cars.</returns>
        public static IEnumerable<Car> GetNonPassengerCars(this Train source)
        {
            if (source.Cars == null)
            {
                throw new ArgumentNullException("Cars");
            }

            return source.Cars.Where(c => !c.IsPassenger);
        }
    }
}
