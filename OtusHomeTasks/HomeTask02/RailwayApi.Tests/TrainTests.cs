using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace RailwayApi.Tests
{
    /// <summary>
    /// Tests for <see cref="Train"/>.
    /// </summary>
    public class TrainTests
    {
        [Fact]
        public void ShouldCreateTrainWithDefaultConstructor()
        {
            Train t = new Train();

            Assert.NotNull(t.Cars);
            Assert.False(t.Cars.Any());
            Assert.Equal(t.CarsLimit, Train.MaximumCarsLimit);
        }

        [Fact]
        public void ShouldFailOnCreateWhenCarsCountLessThanLimit()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var train = new Train(new List<Car>()
                {
                    new Car(),
                    new Car(),
                }, 1);
            });
        }

        [Fact]
        public void ShouldFailOnCreateWhenCarsIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var train = new Train(null, 1);
            });
        }

        [Fact]
        public void ShouldHaveMaximumCarsLimit20()
        {
            Assert.Equal<uint>(20, Train.MaximumCarsLimit);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldHaveIndex(int index)
        {
            Train train = new Train(new List<Car>()
            {
                new Car(10),
                new Car(20)
            }, 5);

            Assert.NotNull(train[index]);
        }

        [Fact]
        public void ShouldExplicitCastFromCar()
        {
            var car = new Car(10);
            var train = (Train)car;

            Assert.NotNull(train);
            Assert.Single(train.Cars);
            Assert.Equal<uint>(1, train.CarsLimit);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(10)]
        public void ShouldEquals(int carsCount)
        {
            Random random = new Random();
            var firstTrainCars = Enumerable.Range(1, carsCount)
                .Select(x => new Car((uint)random.Next(0, 10)))
                .ToList();
            var secondTrainCars = firstTrainCars.ToList();

            var firstTrain = new Train(firstTrainCars, (uint)carsCount);
            var secondTrain = new Train(secondTrainCars, (uint)carsCount);

            Assert.Equal(firstTrain, secondTrain);
            Assert.Equal(firstTrain.GetHashCode(), secondTrain.GetHashCode());
            Assert.True(firstTrain == secondTrain);
        }

        [Fact]
        public void ShouldDiffersIfCarsDiffers()
        {
            var firstTrain = new Train(new List<Car>()
            {
                new Car(),
                new Car()
            }, 5);

            var secondTrain = new Train(new List<Car>()
            {
                new Car()
            }, 5);

            Assert.NotEqual(firstTrain, secondTrain);
            Assert.NotEqual(firstTrain.GetHashCode(), secondTrain.GetHashCode());
        }

        [Fact]
        public void ShouldDiffersIfCarsLimitDiffers()
        {
            var firstTrain = new Train(new List<Car>()
            {
                new Car()
            }, 5);

            var secondTrain = new Train(new List<Car>()
            {
                new Car()
            }, 3);

            Assert.NotEqual(firstTrain, secondTrain);
        }

        [Fact]
        public void ShouldAttachTrain()
        {
            var firstTrain = new Train(new List<Car>()
            {
                new Car()
            }, 5);

            var secondTrain = new Train(new List<Car>()
            {
                new Car()
            }, 3);

            var newTrain = firstTrain + secondTrain;
            Assert.Equal(2, newTrain.Cars.Count());
            Assert.Equal<uint>(2, newTrain.CarsLimit);
        }

        [Fact]
        public void ShouldAttachCar()
        {
            var firstTrain = new Train(new List<Car>()
            {
                new Car(),
                new Car()
            }, 3);

            var car = new Car(10);

            var newTrain = firstTrain + car;
            Assert.Equal(3, newTrain.Cars.Count());
            Assert.Equal<uint>(3, newTrain.CarsLimit);
        }
    }
}
