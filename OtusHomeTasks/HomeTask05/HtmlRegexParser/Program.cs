﻿using HtmlRegexParser.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HtmlRegexParser
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string inputUrl;
            if (args?.Length > 0)
            {
                inputUrl = args[0];
            }
            else
            {
                Console.WriteLine("Input URL:");
                inputUrl = Console.ReadLine();
            }

            try
            {
                Console.WriteLine("Downloading HTML-text...");
                var htmlText = await HttpClientHelper.DownloadHtmlText(inputUrl);
                Console.WriteLine($"HTML-text downloaded, length: {htmlText.Length}");

                var urls = HtmlUrlParser.ParseUrlAddresses(htmlText);
                if (urls.Any())
                {
                    foreach (var url in urls)
                    {
                        Console.WriteLine($"Found URL: {url}");
                    }
                }
                else
                {
                    Console.WriteLine("URLs not found.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex}");
            }

            Console.WriteLine("Job finished.");
        }
    }
}
