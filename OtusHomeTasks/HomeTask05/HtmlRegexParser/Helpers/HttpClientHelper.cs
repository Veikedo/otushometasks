﻿using System.Net.Http;
using System.Threading.Tasks;

namespace HtmlRegexParser.Helpers
{
    public static class HttpClientHelper
    {
        /// <summary>
        /// Async download content from URL.
        /// </summary>
        /// <param name="url">URL.</param>
        /// <returns>Content.</returns>
        public static async Task<string> DownloadHtmlText(string url)
        {
            using var client = new HttpClient();
            return await client.GetStringAsync(url);
        }
    }
}
