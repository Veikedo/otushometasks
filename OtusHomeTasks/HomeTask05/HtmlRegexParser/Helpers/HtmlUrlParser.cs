﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace HtmlRegexParser.Helpers
{
    public static class HtmlUrlParser
    {
        public const string HtmlUrlRegexPattern
            = @"((https?|ftp|file)\://|www.)[A-Za-z0-9\.\-]+(/[A-Za-z0-9\?\&\=;\+!(\)\*\-\._~%]*)*";

        private static readonly Regex htmlUrlRegex 
            = new Regex(HtmlUrlRegexPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase, TimeSpan.FromSeconds(10));

        /// <summary>
        /// Find all URLs in HTML-text.
        /// </summary>
        /// <param name="htmlText">HTML-text.</param>
        /// <returns>URLs.</returns>
        public static IEnumerable<string> ParseUrlAddresses(string htmlText)
        {
            if (string.IsNullOrWhiteSpace(htmlText))
            {
                throw new ArgumentException("HTML-text cannot be null or whitespace.");
            }

            foreach (Match match in htmlUrlRegex.Matches(htmlText))
            {
                yield return match.Value;
            }
        }
    }
}
