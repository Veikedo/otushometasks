using HtmlRegexParser.Helpers;
using HtmlRegexParser.Tests.ClassData;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace HtmlRegexParser.Tests
{
    /// <summary>
    /// Tests for <see cref="HtmlUrlParser"/>.
    /// </summary>
    public class HtmlUrlParserTests
    {
        [Theory]
        [ClassData(typeof(HtmlUrlParserTestData))]
        public void ShouldParseUrlsFromHtml(string input, List<string> expected)
        {
            var actual = HtmlUrlParser.ParseUrlAddresses(input);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldFailWhenInputIsNullOrWhiteSpace(string input)
        {
            Assert.Throws<ArgumentException>(() => HtmlUrlParser.ParseUrlAddresses(input).Any());
        }
    }
}
