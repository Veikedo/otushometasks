﻿using System.IO;

namespace Interfaces.Core.Task1.Serialization
{
    public interface ISerializer
    {
        string Serialize<T>(T item);

        T Deserialize<T>(Stream stream);
    }
}
