﻿using Newtonsoft.Json;
using System.IO;

namespace Interfaces.Core.Task1.Serialization
{
    public class OtusJsonSerializer : ISerializer
    {
        public T Deserialize<T>(Stream stream)
        {
            using StreamReader reader = new StreamReader(stream);
            using JsonTextReader jsonReader = new JsonTextReader(reader);
            JsonSerializer serializer = new JsonSerializer();
            return serializer.Deserialize<T>(jsonReader);
        }

        public string Serialize<T>(T item)
        {
            return JsonConvert.SerializeObject(item);
        }
    }
}
