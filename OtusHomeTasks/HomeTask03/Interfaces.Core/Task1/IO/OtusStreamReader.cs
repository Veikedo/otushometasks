﻿using Interfaces.Core.Task1.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Interfaces.Core.Task1.IO
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream stream;
        private readonly ISerializer serializer;

        public OtusStreamReader(Stream stream, ISerializer serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }

        public void Dispose()
        {
            stream?.Dispose();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var items = serializer.Deserialize<T[]>(stream) ?? Enumerable.Empty<T>();
            foreach (var item in items)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
