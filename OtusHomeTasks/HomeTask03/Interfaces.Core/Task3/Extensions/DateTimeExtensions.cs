﻿using System;

namespace Interfaces.Core.Task3.Extensions
{
    public static class DateTimeExtensions
    {
        public static int GetAge(this DateTime birthDate, DateTime currentDate)
        {
            int years = currentDate.Year - birthDate.Year;
            if ((birthDate.Month > currentDate.Month)
                || (birthDate.Month == currentDate.Month && birthDate.Day > currentDate.Day))
            {
                years--;
            }

            return years;
        }
    }
}
