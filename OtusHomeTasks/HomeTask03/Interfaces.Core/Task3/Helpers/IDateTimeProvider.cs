﻿using System;

namespace Interfaces.Core.Task3.Helpers
{
    public interface IDateTimeProvider
    {
        DateTime Now { get; }
    }
}
