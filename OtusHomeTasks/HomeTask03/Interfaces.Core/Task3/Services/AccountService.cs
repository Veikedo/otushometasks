﻿using Interfaces.Core.Task3.Dtos;
using Interfaces.Core.Task3.Extensions;
using Interfaces.Core.Task3.Helpers;
using Interfaces.Core.Task3.Repositories;
using System;

namespace Interfaces.Core.Task3.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> accountRepository;
        private readonly IDateTimeProvider dateTimeProvider;

        public AccountService(IRepository<Account> accountRepository, IDateTimeProvider dateTimeProvider)
        {
            this.accountRepository = accountRepository;
            this.dateTimeProvider = dateTimeProvider;
        }

        public void AddAccount(Account account)
        {
            ValidateAccount(account);
            accountRepository.Add(account);
        }

        private void ValidateAccount(Account account)
        {
            if (account == null)
            {
                throw new ArgumentNullException(nameof(account));
            }

            if (string.IsNullOrWhiteSpace(account.FirstName))
            {
                throw new ArgumentException("FirstName cannot be null or whitespace.");
            }

            if (string.IsNullOrWhiteSpace(account.LastName))
            {
                throw new ArgumentException("LastName cannot be null or whitespace.");
            }

            if (account.BirthDate.GetAge(dateTimeProvider.Now) < 18)
            {
                throw new ArgumentException("Age cannot be less than 18 years.");
            }
        }
    }
}
