﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        /// <summary>
        /// 1. Вывод информации о заданном аккаунте по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Пользователь.</returns>
        public User GetUser(string login, string password)
        {
            return Users.FirstOrDefault(u => u.Login == login && u.Password == password);
        }

        /// <summary>
        /// 2. Вывод данных о всех счетах заданного пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Результат.</returns>
        public IEnumerable<Account> GetAccounts(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Accounts.Where(a => a.UserId == user.Id).ToList();
        }

        /// <summary>
        /// 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Результат.</returns>
        public IEnumerable<AccountOperationsHistory> GetAccountsWithHistory(User user)
        {
            return GetAccounts(user)
                        .GroupJoin(History,
                        a => a.Id,
                        h => h.AccountId,
                        (a, h) => new AccountOperationsHistory
                        {
                            Account = a,
                            History = h
                        }).ToList();
        }

        /// <summary>
        /// 4. Вывод данных о всех операциях пополнения счёта 
        /// с указанием владельца каждого счёта.
        /// </summary>
        /// <returns>Результат.</returns>
        public IEnumerable<UserOperationsHistory> GetAllInputCashOperations()
        {
            return Users.Select(user => new UserOperationsHistory
            {
                User = user,
                History = GetAccounts(user)
                        .GroupJoin(History,
                        a => a.Id,
                        h => h.AccountId,
                        (a, h) => new AccountOperationsHistory
                        {
                            Account = a,
                            History = h.Where(i => i.OperationType == OperationType.InputCash)
                        })
            }).ToList();
        }

        /// <summary>
        /// 5. Вывод данных о всех пользователях 
        /// у которых на счёте сумма больше N (N задаётся из вне и может быть любой).
        /// </summary>
        /// <param name="n">Сумма на счете.</param>
        /// <returns>Результат.</returns>
        public IEnumerable<User> GetUsersWithCashGreaterThan(decimal n)
        {
            return (from user in Users
                   join account in Accounts on user.Id equals account.UserId
                   where account.CashAll > n
                   select user).ToList();
        }
    }
}