﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class UserOperationsHistory
    {
        public User User { get; set; }

        public IEnumerable<AccountOperationsHistory> History { get; set; }
    }
}
