﻿using System;
using System.Net;
using System.Threading;

namespace TcpChat.Server
{
    class Program
    {
        private static TcpChatServer server;
        private static Thread serverThread;

        static void Main()
        {
            try
            {
                server = new TcpChatServer(IPAddress.Any, 8888);
                server.OnMessageReceived += Server_OnMessageReceived;
                serverThread = new Thread(new ThreadStart(server.Start));
                serverThread.Start();
                Console.WriteLine("Chat server started. Waiting for clients...");
            }
            catch (Exception ex)
            {
                server?.Stop();
                Console.WriteLine($"Chat server stopped due to critical error: {ex.Message}");
            }
        }

        private static void Server_OnMessageReceived(string message)
        {
            Console.WriteLine(message);
        }
    }
}
