﻿using System;
using System.Threading;

namespace TcpChat.Client
{
    class Program
    {
        private const string host = "127.0.0.1";
        private const int port = 8888;

        private static TcpChatClient client;
        private static Thread clientThread;

        static void Main()
        {
            try
            {
                Console.WriteLine("Enter your name:");
                var userName = Console.ReadLine();

                client = new TcpChatClient(userName);
                client.Connect(host, port);
                client.OnMessageReceived += Client_OnMessageReceived;
                clientThread = new Thread(() => SafeThreadStart(client.Start));
                clientThread.Start();

                Console.WriteLine("Chat client started. Press ENTER to send your message.");
                while (true)
                {
                    string inputMessage = Console.ReadLine();
                    client.Send(inputMessage);
                }
            }
            catch (Exception ex)
            {
                client?.Stop();
                Console.WriteLine($"Chat client stopped due to critical error: {ex.Message}");
            }
        }

        private static void SafeThreadStart(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static void Client_OnMessageReceived(string message)
        {
            Console.WriteLine(message);
        }
    }
}
