﻿using System;
using System.Net.Sockets;
using System.Text;
using Chatting.Messaging;

namespace Chatting.Extensions
{
    public static class NetworkStreamExtensions
    {
        public static Message WaitNextMessage(this NetworkStream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            do
            {
                int bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString().FromJson();
        }
    }
}
