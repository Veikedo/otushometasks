﻿namespace Chatting.Messaging
{
    public class Message
    {
        public string User { get; set; }

        public string Text { get; set; }

        public MessageType Type { get; set; }

        public override string ToString()
        {
            return Type switch
            {
                MessageType.Connect => $"{User} has joined the chat.",
                MessageType.Disconnect => $"{User} has left the chat.",
                MessageType.Error => $"Error: {Text}",
                _ => $"{User}: {Text}",
            };
        }
    }
}
