﻿namespace Chatting.Messaging
{
    public enum MessageType
    {
        Default = 0,
        Connect = 1,
        Disconnect = 2,
        Error = 3
    }
}
